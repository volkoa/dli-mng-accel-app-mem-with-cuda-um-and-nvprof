#include <stdio.h>

/*
 * Host function to initialize vector elements. This function
 * simply initializes each element to equal its index in the
 * vector.
 */

void initWithHost(float num, float *a, int N) {
    for (int i = 0; i < N; ++i) {
        a[i] = num;
    }
}

__global__
void initWith(float num, float *a, int N) {
    int index = threadIdx.x + blockIdx.x * blockDim.x;
    int stride = blockDim.x * gridDim.x;

    for (int i = index; i < N; i += stride) {
        a[i] = num;
    }
}

/*
 * Device kernel stores into `result` the sum of each
 * same-indexed value of `a` and `b`.
 */

__global__
void addVectorsInto(float *result, float *a, float *b, int N) {
    int index = threadIdx.x + blockIdx.x * blockDim.x;
    int stride = blockDim.x * gridDim.x;

    for (int i = index; i < N; i += stride) {
        result[i] = a[i] + b[i];
    }
}

/*
 * Host function to confirm values in `vector`. This function
 * assumes all values are the same `target` value.
 */

void checkElementsAre(float target, float *vector, int N) {
    for (int i = 0; i < N; i++) {
        if (vector[i] != target) {
            printf("FAIL: vector[%d] - %0.0f does not equal %0.0f\n", i,
                vector[i], target);
            exit(1);
        }
    }
    printf("Success! All values calculated correctly.\n");
}

int main() {
    const int N = 2 << 24;
    size_t size = N * sizeof(float);

    float *a;
    float *b;
    float *c;

    int deviceId;
    int multiProcessorCount;

    cudaGetDevice(&deviceId);
    // `deviceId` now points to the id of the currently active GPU.

    cudaDeviceProp props;
    cudaGetDeviceProperties(&props, deviceId);
    // `props` now has many useful properties about the active GPU device.
    multiProcessorCount = props.multiProcessorCount;

    cudaMallocManaged(&a, size);
    cudaMallocManaged(&b, size);
    cudaMallocManaged(&c, size);

    // initWithHost(3, a, N);
    // initWithHost(4, b, N);
    // initWithHost(0, c, N);

    size_t threadsPerBlock;
    size_t numberOfBlocks;

    /*
     * nvprof should register performance changes when execution configuration
     * is updated.
     */

    // threadsPerBlock = 256;
    // numberOfBlocks = (N + threadsPerBlock - 1) / threadsPerBlock;
    threadsPerBlock = 256; // 1024;
    numberOfBlocks = 32 * multiProcessorCount;

    printf("Blocks: %d Threads: %d\n", numberOfBlocks, threadsPerBlock);

    initWith<<<numberOfBlocks, threadsPerBlock>>>(3, a, N);
    initWith<<<numberOfBlocks, threadsPerBlock>>>(4, b, N);
    initWith<<<numberOfBlocks, threadsPerBlock>>>(0, c, N);

    cudaError_t addVectorsErr;
    cudaError_t asyncErr;

    addVectorsInto<<<numberOfBlocks, threadsPerBlock>>>(c, a, b, N);

    addVectorsErr = cudaGetLastError();
    if (addVectorsErr != cudaSuccess)
        printf("Error: %s\n", cudaGetErrorString(addVectorsErr));

    asyncErr = cudaDeviceSynchronize();
    if (asyncErr != cudaSuccess)
        printf("Error: %s\n", cudaGetErrorString(asyncErr));

    checkElementsAre(7, c, N);

    cudaFree(a);
    cudaFree(b);
    cudaFree(c);

    return 0;
}
