#!/bin/bash

# launch from src directory
# ./01-vector-add/kernel_config_search.sh

#for iblock in {5..8}; do
for iblock in {1..8}; do
for ith in {5..10}; do
  threads=$((2**$ith))
  # blocks=$((2**$iblock))
  blocks=$((32 * $iblock))
  echo LOOP threads=${threads} blocks=${blocks}
  threads=${threads} blocks=${blocks} envsubst < 01-vector-add/01-vector-add-tmpl.cu \
    > 01-vector-add/01-vector-add-out.cu ;
  # nvcc -arch=sm_70 -o iteratively-optimized-vector-add 01-vector-add/01-vector-add-out.cu -run >compile 2>&1 ;
  nvcc -arch=sm_70 -o iteratively-optimized-vector-add 01-vector-add/01-vector-add-out.cu -run 2>&1
  # nvprof ./iteratively-optimized-vector-add >profile 2>&1

  nvprof ./iteratively-optimized-vector-add  2>&1 | grep addVectorsInto
  # cat compile
  # cat profile
  echo -e '\n\n'
done
done


# optimal seems to be:
#LOOP threads=1024 blocks=128
#Success! All values calculated correctly.
# GPU activities:  100.00%  94.765ms         1  94.765ms  94.765ms  94.765ms  addVectorsInto(float*, float*, float*, int)