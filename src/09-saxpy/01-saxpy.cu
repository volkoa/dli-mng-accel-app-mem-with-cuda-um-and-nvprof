#include <stdio.h>

#ifdef DEBUG
#define CUDA_CALL(F)  if( (F) != cudaSuccess ) \
  {printf("Error %s at %s:%d\n", cudaGetErrorString(cudaGetLastError()), \
   __FILE__,__LINE__); exit(-1);}

#define CUDA_CHECK()  if( (cudaPeekAtLastError()) != cudaSuccess ) \
  {printf("Error %s at %s:%d\n", cudaGetErrorString(cudaGetLastError()), \
   __FILE__,__LINE__-1); exit(-1);}

#else
#define CUDA_CALL(F) (F)
#define CUDA_CHECK()
#endif


#define N 2048 * 2048 // Number of elements in each vector
#define NTHREADS 1024
/*
 * Optimize this already-accelerated codebase. Work iteratively,
 * and use nvprof to support your work.
 *
 * Aim to profile `saxpy` (without modifying `N`) running under
 * 20us.
 *
 * Some bugs have been placed in this codebase for your edification.
 */

__global__
void saxpy(float * a, float * b, float * c) {
    int index = threadIdx.x + blockIdx.x * blockDim.x;
    int stride = blockDim.x * gridDim.x;

//    if (tid < N)
//        c[tid] = 2 * a[tid] + b[tid];

    for (int tid = index; tid < N; tid += stride) {
        c[tid] = 2 * a[tid] + b[tid];
    }

}

__global__ void initWithGPU(float num, float *a, int NN) {
    int index = threadIdx.x + blockIdx.x * blockDim.x;
    int stride = blockDim.x * gridDim.x;

    for (int i = index; i < NN; i += stride) {
        a[i] = num;
    }
}

void checkElementsAre(float target, float *vector, int NN) {
    for (int i = 0; i < NN; i++) {
        if (vector[i] != target) {
            printf("FAIL: vector[%d] - %0.0f does not equal %0.0f\n", i,
                vector[i], target);
            exit(1);
        }
    }
    printf("Success! All values calculated correctly.\n");
}


int main() {
    float *a, *b, *c;

    int size = N * sizeof(float); // The total number of bytes per vector

    int deviceId;
    int multiProcessorCount;
    cudaDeviceProp props;

    cudaGetDevice(&deviceId);
    cudaGetDeviceProperties(&props, deviceId);
    // `props` now has many useful properties about the active GPU device.
    multiProcessorCount = props.multiProcessorCount;

    cudaMallocManaged(&a, size);
    cudaMallocManaged(&b, size);
    cudaMallocManaged(&c, size);

    // Initialize memory
//    for (int i = 0; i < N; ++i) {
//        a[i] = 2;
//        b[i] = 1;
//        c[i] = 0;
//    }

    CUDA_CALL(cudaMemPrefetchAsync(a, size, deviceId));
    CUDA_CALL(cudaMemPrefetchAsync(b, size, deviceId));
    CUDA_CALL(cudaMemPrefetchAsync(c, size, deviceId));

//    int threads_per_block = 128;
//    int number_of_blocks = (N / threads_per_block) + 1;
    int threads_per_block = NTHREADS; // 1024;
    int number_of_blocks = 32 * multiProcessorCount;

    initWithGPU<<<number_of_blocks, threads_per_block>>>(2, a, N);
    CUDA_CHECK()
    initWithGPU<<<number_of_blocks, threads_per_block>>>(1, b, N);
    CUDA_CHECK()
    initWithGPU<<<number_of_blocks, threads_per_block>>>(0, c, N);
    CUDA_CHECK()

    cudaError_t kernelErr;
    cudaError_t asyncErr;

    saxpy <<< number_of_blocks, threads_per_block >>> ( a, b, c);

    kernelErr = cudaGetLastError();
    if (kernelErr != cudaSuccess)
        printf("Error: %s\n", cudaGetErrorString(kernelErr));

    asyncErr = cudaDeviceSynchronize();
    if (asyncErr != cudaSuccess)
        printf("Error: %s\n", cudaGetErrorString(asyncErr));

    cudaMemPrefetchAsync(c, size, cudaCpuDeviceId);

    // checkElementsAre(5, c, N);

    // Print out the first and last 5 values of c for a quality check
    for (int i = 0; i < 5; ++i)
        printf("c[%d] = %.1f, ", i, c[i]);
    printf("\n");
    for (int i = N - 5; i < N; ++i)
        printf("c[%d] = %.1f, ", i, c[i]);
    printf("\n");

    cudaFree(a);
    cudaFree(b);
    cudaFree(c);

    return 0;
}
